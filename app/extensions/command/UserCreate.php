<?php
namespace app\extensions\command;

use app\models\Users;

class UserCreate extends \lithium\console\Command {

	public $u;	//username
	public $p;	//password

    public function run() {
        $this->header('User Creator');

        if(empty($this->u)){
        	$this->out('Error: Please run with --u=USERNAME to include the username');
        	return false;
        }
        if(empty($this->p)){
        	$this->out('Error: Please run with --p=PASSWORD to include password');
        	return false;
        }

        $this->out('Creating an user...');
        
        $user = Users::create();
        $user->username = $this->u;
        $user->password = $this->p;
        if($user->save()){
        	$this->out('Created user ' . $user->username);	
        } else {
        	$this->out('Error creating user');	
        }
        
    }
}


/* To create Administrator from CLI:
 
 $ ./libraries/lithium/console/li3             
...
COMMANDS via app
    administrator-create

$ ./libraries/lithium/console/li3 administrator-create --u=USERNAME --p=PASSWORD


 */


?>
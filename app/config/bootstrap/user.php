<?php

/*
 * Define Auth Types
 */
use lithium\security\Auth;
use app\models\Administrators;
use lithium\analysis\Logger;
use lithium\storage\Session;

/* -- Auth::config defined in bootstrap/session.php --*/

/**
* Apply filter to authenticate all actions, with exceptions defined in the publicActions property
*/
use lithium\action\Dispatcher;
use lithium\action\Response;
// use lithium\security\Auth;

Dispatcher::applyFilter('_callable', function($self, $params, $chain) {
	$ctrl    = $chain->next($self, $params, $chain);
	$request = isset($params['request']) ? $params['request'] : null;
	$action  = $params['params']['action'];

	// All controller actions will be logged in User actions unless:
	// publicActions - doesn't require login
	// userActions - only admin can access 

	// Forward request if early part in the chain
	if( $ctrl instanceof Closure){
		return $ctrl;
	}

	//Logger::alert(print_r($ctrl->userActions));

	if ( isset( $ctrl->userActions ) && in_array($action, $ctrl->userActions) ) {
		if (Auth::check('user')) {
			//Proceed if logged in as admin & action is defined in userActions
			//$params['adminActionAuthenticated'] = true;
			Session::write('userActionAuthenticated', 1);
			return $ctrl;
		} else {
			// Prompt for admin login
			return function() use ($request) {
				return new Response(compact('request') + array('location' => 'Users::login'));
			};
		}
	}

	//Not an admin action, proceed to User auth check
	return $ctrl;
});

?>
<?php

namespace app\models;

class Modules extends \lithium\data\Model {
    public $hasOne = [
        "Subscription" => [
            "to" => "Subscriptions",
            "key" => [
                "id" => "id_module"// field in "from" model => field in "to" model
            ]
        ]];
    
    public $hasMany = [
        "Subscriptions" => [
            "to" => "Subscriptions",
            "key" => [
                "id" => "id_module"// field in "from" model => field in "to" model
            ]
        ],
      "Components" => [
            "to" => "Components",
            "key" => [
                "id" => "id_module"// field in "from" model => field in "to" model
            ]
        ]  
    ];
	public $validates = array();
}

Modules::applyFilter('save', function($self, $params, $chain) {
    
    // If data is passed to the save function, set it in the record.  
    // This makes it possible to validate the data before continuing the save process.
    if(!empty($params['data'])) {
        $data = $params['data'];
        $params['entity']->set($data);
        $params['data'] = array();
    }
    
    // Assign the record to a variable for easy access
    $record = $params['entity'];

    // Put your "before save" code here.
    if(!$record->exists()) {
        $record->created = date('Y-m-d h:i:s');
    } else {
    }
    $record->modified = date('Y-m-d h:i:s');
    
    // Put the record back in the $params array and continue the save process
    $params['entity'] = $record;
    $result = $chain->next($self, $params, $chain);

    // Put your "after save" code here
    if($result) {
       //log('Success!');
    }

    return $result;
});

?>
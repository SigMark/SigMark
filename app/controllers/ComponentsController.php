<?php

namespace app\controllers;

use app\models\Modules;
use app\models\Marks;
use app\models\Components;
use lithium\action\DispatchException;

class ComponentsController extends \lithium\action\Controller {

    //All actions default as User auth-required actions
    public $publicActions = array('');
    public $userActions = array('add', 'edit', 'delete');

	public function add() {
		$edit = false;
		$module = Modules::first($this->request->id);
		if (!$module) {
			return $this->redirect('Modules::index');
		}
		
		$component = Components::create();

		if ($this->request->data) {
			$this->request->data["id_module"] = $module->id; //Add module id

			if ($component->save($this->request->data))
				return $this->redirect(array('Modules::view', 'args' => array($module->id)));

		}
		return compact('edit', 'module', 'component');
	}

	public function edit() {
		$edit = true;
		
		$component = Components::find($this->request->id);
		if (!$component) {
			return $this->redirect('Modules::index');
		}
		
		$module = Modules::first($component->id_module);

		if (!$module) {
			return $this->redirect('Modules::index');
		}
		
		if ($this->request->data) {
			$this->request->data["id_module"] = $module->id; //Add module id

			if ($component->save($this->request->data)) 
				return $this->redirect(array('Modules::view', 'args' => array($module->id)));
		}
		$this->_render['template'] = 'add';
		return compact('edit', 'module', 'component');
	}

	public function delete() {
		$component = Components::find($this->request->id);
		if (!$component)
			return $this->redirect('Modules::index');
		
		Marks::remove(["id_component" => $component->id]); //Remove marks
			
		$component->delete();
		return $this->redirect(array('Modules::view', 'args' => array($component->id_module)));
	}
}

?>
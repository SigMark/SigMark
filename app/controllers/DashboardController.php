<?php

namespace app\controllers;

use app\models\Dashboard;
use app\models\Users;
use app\models\Modules;
use app\models\Components;
use app\models\Marks;
use app\models\Subscriptions;
use lithium\storage\Session;
use lithium\action\DispatchException;

class DashboardController extends \lithium\action\Controller {

    //All actions default as User auth-required actions
    public $publicActions = array();
    public $userActions = array('index');

	public function index() {
		if (Session::read('userRole') == 0) { //Student
			$userId = Session::read("userId");
			$nbModules = Subscriptions::count([
				"id_user" => $userId
			]);
			
			$modules = Modules::find('all', [
				'conditions' => [
					"Subscriptions.id_user" => $userId
				],
				'with' => [
					'Subscriptions',
					'Components'
				]
			]);
			
			$nbMarks = Marks::find('first',[
	        	"id_user" => $userId
	        ]);
			
			foreach($modules as $module){
			    $module->sum = 0;
				$module->failed = false;
				
			    foreach($module->components as $component){
			    	$component->hasMarks = false; 

			        $mark = Marks::find('first',[
			        	"id_component" => $component->id,
			        	"id_user" => $userId
			        ]);
			        if ($mark) { //student has a mark
			        	if($mark->point > 40)
			        		$module->failed = true;
			        		
			        	$component->mark = $mark;
			        	$module->sum += ($mark->point * $component->percent);
			        	$component->hasMarks = true;
			        }
			    }
			    $module->sum = $module->sum / 100;
			}
			
			$this->_render["template"] = "index_student";
			return compact("modules", "nbModules", "nbMarks");
		}
		else { //Teacher
		    $nbStudents = Users::count([
				"role" => 0
			]);
			
			$nbModules = Modules::count();
			
			$nbComponents = Components::count();
		
		    return compact("nbStudents", "nbModules", "nbComponents");
		}
	}
}

?>
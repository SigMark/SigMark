<?php

namespace app\controllers;

use lithium\security\Auth;
use app\models\Users;
use app\models\Marks;
use app\models\Subscriptions;
use lithium\storage\Session;
use lithium\action\DispatchException;
use lithium\analysis\Logger;

class UsersController extends \lithium\action\Controller {

    //All actions default as User auth-required actions
    public $publicActions = array('login','logout');
    public $userActions = array('index', 'add', 'view', 'edit', 'delete');

    protected function _init(){
        //To deal with media/json requests
        $this->_render['negotiate'] = true;
        parent::_init();
    }

	public function index() {
		$student = false;
		$users = null;
		
		if (!isset($this->request->args[0]))
			return $this->redirect('Dashboard::index');
			
		switch ($this->request->args[0]) {
			case 'students':
				$student = true; 
				$users = Users::find('all', 
					[
						'conditions' => ['role' => '0']
					]);
				break;
			
			case 'teachers':
				$student = false;
				$users = Users::find('all', 
					[
						'conditions' => ['role' => '1']
					]);
				break;
			
			default;
				return $this->redirect('Dashboard::index');
				break;
		}

		return compact('users', 'student');
	}

	public function view() {
		$user = Users::first($this->request->id);
		if (!$user) {
			return $this->redirect('Users::index');
		}
		
		if (($this->request->data) && $user->save($this->request->data)) {
			return $this->redirect(array('Users::view', 'args' => array($user->id)));
		}
		
		return compact('user');
	}
	
	public function profile() {
		$user = Users::first(Session::read('userId'));
		if (!$user) {
			return $this->redirect('Dashboard::index');
		}
		
		if (($this->request->data) && $user->save($this->request->data)) {
			return $this->redirect('Users::profile');
		}
		
		$this->_render['template'] = 'view';
		return compact('user');
	}

	public function add() {
		$user = Users::create();

		if ($this->request->data){
			if ($this->request->data["gender"] == 0)
				$gender = "male";
			else
				$gender = "female";
			
			//Random Picture API
			try {
				$json = file_get_contents("https://randomuser.me/api/?gender=" . $gender);
				$result = json_decode($json);
				$this->request->data["img_url"] = $result->results[0]->picture->large; //Get the img url
			}
			catch(Exception $e) {
				//Nothing to throw
			}
			
			//Save
			if($user->save($this->request->data))
				return $this->redirect('Users::index');
		}
		return compact('user');
	}

	public function delete() {
		$user = Users::find($this->request->id);
		if (!$user)
			return $this->redirect('Users::index');
			
		Subscriptions::remove([
			"id_user" => $user->id
		]);
		
		Marks::remove([
			"id_user" => $user->id
		]);
		
		$user->delete();
		
		return $this->redirect('Users::index');
	}

    public function login() {

		$authUser = Auth::check('user', $this->request);
        if ($authUser) {
        	$user = Users::find('first', 
        		[
					'conditions' => ['username' => $authUser["username"]]
				]);
			if (!$user)
				break;
			//Save user data to session
			Session::write('userUsername', $user->username);
			Session::write('userId', $user->id);
			Session::write('userRole', $user->role);
			Session::write('userFirstName', $user->firstname);
			Session::write('userLastName', $user->lastname);
			Session::write('userImgUrl', $user->img_url);
            
            return $this->redirect('/');
        }

		$loginFailed = false;
        if ($this->request->data){
            $loginFailed = true;
        }
        $this->_render['layout'] = 'login';
        return compact('loginFailed');

    }

    public function logout() {
        Auth::clear('user');
		Session::delete('userActionAuthenticated');
        return $this->redirect('/');
    }
}

?>
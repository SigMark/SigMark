<?php

namespace app\controllers;

use app\models\Marks;
use app\models\Modules;
use app\models\Users;
use app\models\Components;
use app\models\Subscriptions;
use lithium\action\DispatchException;

class MarksController extends \lithium\action\Controller {

    //All actions default as User auth-required actions
    public $publicActions = array('');
    public $userActions = array('index', 'add', 'view', 'edit', 'delete');

	public function index() {
		$marks = Marks::all();
		
		return compact('marks');
	}

	public function view() {
		$mark = Marks::first($this->request->id);
		return compact('mark');
	}

	public function add() {
		$component = Components::first($this->request->id);
		if (!$component) {
			return $this->redirect('Modules::index');
		}
		
		if ($this->request->data){
			if (isset($this->request->data["marks"]) && isset($this->request->data["already"]))
				foreach($this->request->data["marks"] as $key => $value){ //For each marks
					
					if (isset($value) && $value != "" && is_numeric($value)){ //Value set
					
						if ($this->request->data["already"][$key] == 0) //Check if record already exist
							$mark = Marks::create(); //Didn't exist : create
							
						else
							$mark = Marks::find('fist', [ //Already exists : find
								'conditions' => [
									'id_user' => $key,
									'id_component' => $component->id
								]
							]);
							if ($mark) {
								$mark->point = $value;
								$mark->id_user = $key;
								$mark->id_component = $component->id;
								
								$mark->save();
							}
					}
					
				}

		}
		
		$module = Modules::first($component->id_module);
		if (!$module) {
			return $this->redirect('Modules::index');
		}
		
		//Get Users surbscribed from the component's module and mark if exist
		$users = Users::find('all', [
			'conditions' => [
				"Subscriptions.id_module" => $component->id_module
				//Cannot select marks only from this module because the mark could not exist.
			],
			'with' => [
				'Subscriptions',
				'Marks'
			]
		]);
		
		return compact('module', 'component', 'users');
	}

	public function edit() {
		$mark = Marks::find($this->request->id);

		if (!$mark) {
			return $this->redirect('Marks::index');
		}
		if (($this->request->data) && $mark->save($this->request->data)) {
			return $this->redirect(array('Marks::view', 'args' => array($mark->_id)));
		}
		return compact('mark');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Marks::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Marks::find($this->request->id)->delete();
		return $this->redirect('Marks::index');
	}
}

?>
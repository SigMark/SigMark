<?php

namespace app\controllers;

use app\models\Profile;
use lithium\action\DispatchException;

class ProfileController extends \lithium\action\Controller {

    //All actions default as User auth-required actions
    public $publicActions = array('');
    public $userActions = array('index', 'add', 'view', 'edit', 'delete');

	public function index() {
		$profiles = Profile::all();
		return compact('profiles');
	}

	public function view() {
		$profile = Profile::first($this->request->id);
		return compact('profile');
	}

	public function add() {
		$profile = Profile::create();

		if (($this->request->data) && $profile->save($this->request->data)) {
			return $this->redirect(array('Profile::view', 'args' => array($profile->_id)));
		}
		return compact('profile');
	}

	public function edit() {
		$profile = Profile::find($this->request->id);

		if (!$profile) {
			return $this->redirect('Profile::index');
		}
		if (($this->request->data) && $profile->save($this->request->data)) {
			return $this->redirect(array('Profile::view', 'args' => array($profile->_id)));
		}
		return compact('profile');
	}

	public function delete() {
		if (!$this->request->is('post') && !$this->request->is('delete')) {
			$msg = "Profile::delete can only be called with http:post or http:delete.";
			throw new DispatchException($msg);
		}
		Profile::find($this->request->id)->delete();
		return $this->redirect('Profile::index');
	}
}

?>
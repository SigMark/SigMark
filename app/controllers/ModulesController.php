<?php

namespace app\controllers;

use app\models\Modules;
use app\models\Components;
use app\models\Subscriptions;
use app\models\Users;
use app\models\Marks;
use lithium\action\DispatchException;

class ModulesController extends \lithium\action\Controller {

    //All actions default as User auth-required actions
    public $publicActions = array('');
    public $userActions = array('index', 'add', 'view', 'edit', 'delete');

	public function index() {
		$modules = Modules::all();
		foreach($modules as $module) {
			$module->nbComponents = Components::find('count', 
				[
					'conditions' => ['id_module' => $module->id]
				]);
		
			$module->nbStudents = Subscriptions::find('count', 
				[
					'conditions' => ['id_module' => $module->id]
				]);
		}
		
		return compact('modules');
	}

	public function view() {
		$module = Modules::first($this->request->id);
		if (!$module) {
			return $this->redirect('Modules::index');
		}
		$components = Components::find('all', 
			[
				'conditions' => ['id_module' => $module->id]
			]);
		$module->nbStudents = Subscriptions::find('count', 
			[
				'conditions' => ['id_module' => $module->id]
			]);
			
		foreach($components as $component){
			$component->countUserMarked = Users::find('count', [
				'conditions' => [
					"Subscriptions.id_module" => $component->id_module,
					"Marks.id_component" => $component->id
				],
				'with' => [
					'Subscriptions',
					'Marks'
				]
			]);
		}
		
		/*$users = Users::find('all', [
			'conditions' => [
				"Subscriptions.id_module" => $component->id_module,
			],
			'with' => [
				'Subscriptions',
				'Marks'
			]
		]);*/
		
		return compact('module', 'components');
	}

	public function add() {
		$edit = false;
		$module = Modules::create();
		
		if (($this->request->data) && $module->save($this->request->data)) {
			return $this->redirect('Modules::index');
		}
		return compact('edit', 'module');
	}
	
	//List / Add users to a module
	public function users() {
		$module = Modules::first($this->request->id);
		if (!$module) {
			return $this->redirect('Modules::index');
		}
		
		if ($this->request->data){ //Process post
		
			if (isset($this->request->data["addStudents"])) { //List of usersId
				
				foreach($this->request->data["addStudents"] as $userId) { //Create a Subscription for each user
					if(is_numeric($userId)) {
						$subscription = Subscriptions::create();
						$subscription->id_user = $userId;
						$subscription->id_module = $module->id;
						
						$subscription->save();
					}
				}
			}
			
		}
		
		$subscriptions = Subscriptions::find('all', 
			[
				'fields' => [
					"id_module",
					"id_user"
				],
				'conditions' => ['id_module' => $module->id]
			]);
		
		//Enrolled students
		$users = [];	
		foreach($subscriptions as $subscription){
			$users += Users::find('all', 
			[
				'conditions' => ['id' => $subscription->id_user]
			])->to('array');	
		}

		$notEnrolledStudents = Users::find('all', 
			[
				'conditions' => [
					"role" => 0	
				]
			])->to('array');
			
		//Get subscriptions user ids
		$subscriptionIds = [];
		foreach($subscriptions as $subscription){
			$subscriptionIds[] = $subscription->id_user;
		}

		foreach($notEnrolledStudents as $key => $value){ //Remove already existing students
			if (in_array($notEnrolledStudents[$key]["id"], $subscriptionIds)) {
				unset($notEnrolledStudents[$key]);
			}
			else {
				$notEnrolledStudents[$key]["name"] = $notEnrolledStudents[$key]["firstname"] . " " . $notEnrolledStudents[$key]["lastname"];
			}
		}
		

		return compact('module', 'users', 'notEnrolledStudents');
	}
	
	public function deleteUser(){
		if (sizeof($this->request->args) != 2) //2 parameters required
			return $this->redirect('Modules::index');
		$subscription = Subscriptions::remove([
				"id_user" => $this->request->args[1],
				"id_module" => $this->request->args[0]
			]);
		if (!$subscription) {
			return $this->redirect('Modules::index');
		}

		return $this->redirect(array('Modules::users', 'args' => array($this->request->args[0])));
	}

	public function edit() {
		$edit = true;
		
		$module = Modules::first($this->request->id);
		if (!$module) {
			return $this->redirect('Modules::index');
		}
		if (($this->request->data) && $module->save($this->request->data)) {
			return $this->redirect(array('Modules::view', 'args' => array($module->_id)));
		}
		
		$this->_render['template'] = 'add';
		return compact('edit', 'module');
	}

	public function delete() {
		$module = Modules::find($this->request->id);
		if (!$module)
			return $this->redirect("Modules::index");
			
		Subscriptions::remove([
			"id_module" => $module->id
		]);
		
		$components = Components::find('all', 
			[
				'conditions' => ["id_module" => $module->id]
			]
		);
		if ($components)
			foreach($components as $component){
				Marks::remove([
					"id_component" => $component->id
				]);	
				$component->delete();
			}
		
		$module->delete();
		return $this->redirect('Modules::index');
	}
}

?>